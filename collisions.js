/*jslint white: true */

/* Collisions */
/***********************************/
var floatingPoints = [];
floatingPointCycleDuration = 50;

function checkCollisions() {
}


function checkPlayerCollisionWithEnemy(targets) {
  for (i = 0; i < targets.length; i += 1) {
    if (player.crashWith(targets[i])) {
      killPlayer();
      if (lives > 0) {
        return;
      }
      showGameOver();
    }
  }
}

function killPlayer() {
  died = true;
  lives -= 1;
}

function showGameOver() {
  gameArea.stop();
  gameOver.text = "Game Over";
  gameOver.update();
}

function addNewFloatingPoint(x, y, points, action) {
  symbol = "+";
  color = "black";
  if (action == "lose") {
    symbol = "-";
    color = "red";
  }
  newPoint = new component("20px", "Consolas", color, x, y, "text");
  newPoint.text = symbol + points;
  newPoint.cycleNumber = 0;
  floatingPoints.push(newPoint);
}

function updateFloatingPoints() {
  for (i = 0; i < floatingPoints.length; i += 1) {
    floatingPoints[i].cycleNumber += 1;
    floatingPoints[i].y -= 1;
    floatingPoints[i].update();
    if (floatingPoints[i].cycleNumber > floatingPointCycleDuration) {
      floatingPoints.splice(i, 1);
    }
  }
}
