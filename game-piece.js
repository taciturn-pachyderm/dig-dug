var gamePieceSpeed = 0.5;
var maxGamePieceSpeed = 4;
var friction = 0.8;
var gamePieceSide = gridSquareSide;
var gamePieceWidth = gamePieceSide;
var gamePieceHeight = gamePieceSide;

function manageForces(bird) {
  bird.speedX *= friction;
  bird.speedY *= friction;
}

function checkBlocks(gamePiece) {
  for (var i = 0; i < blocks.length; i++) {
    if (blocks.diggable === null) {
      return;
    }
    var dir = gamePiece.getCollisionDirection(blocks[i]);

    if (dir === "left" && gamePiece.directionX > 0 || dir === "right" && gamePiece.directionX < 0) {
      gamePiece.speedX = 0;
      break;
    }
    // else if (dir === "bottom" || dir === "top") {
    if (dir === "bottom" && gamePiece.directionY < 0 || dir === "top" && gamePiece.directionY > 0) {
      gamePiece.speedY = 0;
      break;
    }
  }
}

function checkImpact(gamePiece, targets) {
  for (var i = 0; i < targets.length; i++) {
    if (targets[i].diggable != null) {
      return;
    }
    var dir = targets[i].getCollisionDirection(gamePiece);
    // console.log(format("{0} hit {1} on the {2}!", gamePiece.type, targets[i].type, dir));
    if (dir === "right" && gamePiece.directionX === -1) {
      targets[i].width -= gamePieceSpeed;
      return;
    }
    if (dir === "left" && gamePiece.directionX === 1) {
      targets[i].width -= gamePieceSpeed;
      targets[i].x += gamePieceSpeed;
      return;
    }
    if (dir === "top" && gamePiece.directionY === 1) {
      targets[i].height -= gamePieceSpeed;
      targets[i].y += gamePieceSpeed;
      return;
    }
    if (dir === "bottom" && gamePiece.directionY === -1) {
      targets[i].height -= gamePieceSpeed;
      return;
    }
  }
}
