/*jslint white: true */

function component(width, height, color, x, y, type, extra1, extra2) {
  this.gamearea = gameArea;
  this.remove = false;
  this.type = type;
  this.width = width;
  this.height = height;
  this.speedX = 0;
  this.speedY = 0;
  this.x = x;
  this.y = y;
  this.update = function() {
    ctx = gameArea.context;
    ctx.fillStyle = color;
    if (this.type == "text") {
      ctx.font = this.width + " " + this.height;
      ctx.fillText(this.text, this.x, this.y);
    } else if (this.type == "laser") {
      this.speedX = extra1;
      this.speedY = extra2;
      ctx.fillRect(this.x, this.y, this.width, this.height);
    } else if (this.type === "player" || this.type === "enemy") {
      this.makeATriangle();
    } else {
      ctx.fillRect(this.x, this.y, this.width, this.height);
    }
  };
  this.makeATriangle = function() {
    ctx.beginPath();
    vertices = this.getTriangleVertices();
    ctx.moveTo(vertices['x1'], vertices['y1']);
    ctx.lineTo(vertices['x2'], vertices['y2']);
    ctx.lineTo(vertices['x3'], vertices['y3']);
    ctx.fill();
  }
  this.getTriangleVertices = function() {
    if (this.directionX > 0) {
      return getRightTriangle(ctx, this);
    }
    if (this.directionX < 0) {
      return getLeftTriangle(ctx, this);
    }
    if (this.directionY > 0) {
      return getDownTriangle(ctx, this);
    }
    if (this.directionY < 0) {
      return getUpTriangle(ctx, this);
    }
  }
  this.newPos = function() {
    this.x += this.speedX;
    this.y += this.speedY;
  };
  this.crashWith = function(otherObject) {
    var crash = true;
    if (this.getBottom() < otherObject.getTop() || this.getTop() > otherObject.getBottom() || this.getRight() < otherObject.getLeft() || this.getLeft() > otherObject.getRight()) {
      crash = false;
    }
    return crash;
  };
  this.getCollisionDirection = function(otherObject) {
    // get the vectors to check against
    var vectorX = (this.x + (this.width / 2)) - (otherObject.x + (otherObject.width / 2));
    var vectorY = (this.y + (this.height / 2)) - (otherObject.y + (otherObject.height / 2));
    // add the half widths and half heights of the objects
    var halfWidths = (this.width / 2) + (otherObject.width / 2);
    var halfHeights = (this.height / 2) + (otherObject.height / 2);
    var collisionDirection = null;

    // if the x and y vector are less than the half width or half height, they we must be inside the object, causing a collision
    if (Math.abs(vectorX) < halfWidths && Math.abs(vectorY) < halfHeights) {
      // figures out on which side we are colliding (top, bottom, left, or right)
      // the offset is to reset this object to the surface of the otherObject
      var offsetX = halfWidths - Math.abs(vectorX);
      var offsetY = halfHeights - Math.abs(vectorY);
      if (offsetX >= offsetY) {
        if (vectorY > 0) {
          collisionDirection = "top";
          if (this.type != "block") {
            this.y += offsetY;
          }
        } else {
          collisionDirection = "bottom";
          if (this.type != "block") {
            this.y -= offsetY;
          }
        }
      } else {
        if (vectorX > 0) {
          collisionDirection = "left";
          if (this.type != "block") {
            this.x += offsetX;
          }
        } else {
          collisionDirection = "right";
          if (this.type != "block") {
            this.x -= offsetX;
          }
        }
      }
    }
    return collisionDirection;
  };
  this.getMiddleX = function() {
    return this.x + this.width / 2;
  };
  this.getMiddleY = function() {
    return this.y + this.height / 2;
  };
  this.getTop = function() {
    return this.y;
  };
  this.getBottom = function() {
    return this.y + this.height;
  };
  this.getLeft = function() {
    return this.x;
  };
  this.getRight = function() {
    return this.x + this.width;
  };
}

function getRightTriangle(ctx, myObject) {
  vertices = {};
  vertices['x1'] = myObject.x;
  vertices['y1'] = myObject.y;
  vertices['x2'] = myObject.x + myObject.width;
  vertices['y2'] = myObject.y + myObject.height/2;
  vertices['x3'] = myObject.x;
  vertices['y3'] = myObject.y + myObject.height;
  return vertices;
}

function getLeftTriangle(ctx, myObject) {
  vertices = {};
  vertices['x1'] = myObject.x + myObject.width;
  vertices['y1'] = myObject.y;
  vertices['x2'] = myObject.x;
  vertices['y2'] = myObject.y + myObject.height/2;
  vertices['x3'] = myObject.x + myObject.width;
  vertices['y3'] = myObject.y + myObject.height;
  return vertices;
}

function getDownTriangle(ctx, myObject) {
  vertices = {};
  vertices['x1'] = myObject.x;
  vertices['y1'] = myObject.y;
  vertices['x2'] = myObject.x + myObject.width/2;
  vertices['y2'] = myObject.y + myObject.height;
  vertices['x3'] = myObject.x + myObject.width;
  vertices['y3'] = myObject.y;
  return vertices;
}

function getUpTriangle(ctx, myObject) {
  vertices = {};
  vertices['x1'] = myObject.x;
  vertices['y1'] = myObject.y + myObject.height;
  vertices['x2'] = myObject.x + myObject.width/2;
  vertices['y2'] = myObject.y;
  vertices['x3'] = myObject.x + myObject.width;
  vertices['y3'] = myObject.y + myObject.height;
  return vertices;
}
