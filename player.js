/*jslint white: true */

/* Player */
/***********************************/

var playerSide = gamePieceSide * 0.5;

var playerStartX = (canvasWidth - gamePieceWidth) / 2;
var playerStartY = gridSquareSide;
var player = new component(playerSide, playerSide, "blue", playerStartX, playerStartY, "player");
player.directionX = 1;
player.score = 0;
player.startX = playerStartX;
player.startY = playerStartY;

function managePlayer() {
  checkImpact(player, blocks);
  movePlayer();
  checkBlocks(player);
  updatePlayer();
}

function updatePlayer() {
  player.newPos();
  player.update();
}

function movePlayer() {
  manageForces(player);
  setMovementParameters(player);
}

function setMovementParameters() {
  if (!gameArea.keys) {
    return;
  }
  // left
  if (gameArea.keys[65]) {
    player.directionX = -1;
    player.directionY = 0;
    // move it
    player.speedX = -gamePieceSpeed;
    return;
  }
  // right
  if (gameArea.keys[68]) {
    player.directionX = 1;
    player.directionY = 0;
    // move it
    player.speedX = gamePieceSpeed;
    return;
  }
  // up
  if (gameArea.keys[87] && player.y > gridSquareSide) {
    player.directionX = 0;
    player.directionY = -1;
    // move it
    player.speedY = -gamePieceSpeed;
    return;
  }
  // down
  if (gameArea.keys[83]) {
    player.directionX = 0;
    player.directionY = 1;
    // move it
    player.speedY = gamePieceSpeed;
    return;
  }
}
