/*jslint white: true */

/* Blocks */
/***********************************/

var blocksDict = [];
var blocks = [];
var blockPointValue = 1;
var innerBlockLayerTop = gridSquareSide * 3;
var outerBlockLayer = {}
var maxBlocks = 100;
var blockSide = gamePieceSide; // * 0.8;
var undiggableBlockCount = 0;

function manageBlocks() {
  if (gameArea.frameNo == 1) {
    outerBlockLayer["top"] = gridSquareSide * 2;
    outerBlockLayer["left"] = 0;
    outerBlockLayer["right"] = Math.max.apply(null, xVertices) - gridSquareSide;
    outerBlockLayer["bottom"] = Math.max.apply(null, yVertices) - gridSquareSide;
    spawnBlocks(maxBlocks);
    convertBlocksDictToArray();
    makeSomeBlocksUndiggable();
    makeOuterLayerUndiggable();
  }
  removeDestroyedBlocks();
  updateBlocks();
}

function makeOuterLayerUndiggable() {
  // top and bottom rows
  for (var x = 0; x < outerBlockLayer["right"] + gridSquareSide; x += gridSquareSide) {
    // don't insert the middle 2 blocks
    if (x < xVertices[Math.floor(xVertices.length/2)] - gridSquareSide || x >= xVertices[Math.ceil(xVertices.length/2)] + gridSquareSide) {
      makeUndiggableBlock(x, outerBlockLayer["top"]);
    }
    makeUndiggableBlock(x, outerBlockLayer["bottom"]);
  }
  // sides
  for (var y = 0; y < outerBlockLayer["bottom"] + gridSquareSide; y += gridSquareSide) {
    makeUndiggableBlock(outerBlockLayer["left"], y);
    makeUndiggableBlock(outerBlockLayer["right"], y);
  }
}

function makeSomeBlocksUndiggable() {
  for (var i = 0; i < blocks.length; i++) {
    if (undiggableBlockCount < maxBlocks / 5) {
      makeUndiggableBlock(blocks[i].x, blocks[i].y);
      blocks.splice(i, 1);
      undiggableBlockCount++;
    }
  }
}

function makeUndiggableBlock(x, y, index) {
  newBlock = new component(blockSide, blockSide, "grey", x, y, "block");
  newBlock.diggable = false;
  blocks.push(newBlock);
}

function removeDestroyedBlocks() {
  for (var i = 0; i < blocks.length; i++) {
    if (blocks[i].width <= blockSide / 20 || blocks[i].height <= blockSide / 20) {
      blocks.splice(i, 1);
    }
  }
}

function convertBlocksDictToArray() {
  blocks = Object.values(blocksDict);
}

function spawnBlocks(amount) {
  // use a dict to prevent duplication
  while (Object.keys(blocksDict).length < amount) {
    x = xVertices[Math.floor(Math.random() * xVertices.length)];
    y = yVertices[Math.floor(Math.random() * yVertices.length)];
    if (x > outerBlockLayer["left"] && x < outerBlockLayer["right"] && y > outerBlockLayer["top"] && y < outerBlockLayer["bottom"]) {
      blocksDict[x+":"+y] = (generateBlock(x, y));
    }
  }
}

function updateBlocks() {
  for (i = 0; i < blocks.length; i += 1) {
    blocks[i].update();
  }
}

function generateBlock(x, y) {
  block = new component(blockSide, blockSide, "peru", x, y, "block");
  block.pointValue = currentLevel;
  block.hitPoints = 4;
  return block;
}
